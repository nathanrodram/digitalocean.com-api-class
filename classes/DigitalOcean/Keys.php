<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author    Nathan Rodriguez nathan.rodram at gmail.com
 * 
 */

namespace DigitalOcean;

class Keys extends \DigitalOcean\RestService {

    protected $controller = 'ssh_keys';

    public function getSshKeys() {
        /* List ssh keys
         * @return array
         */
        return $this->sendCall();
    }

    public function getSshKey($keyId) {
        /* Show ssh key content
         * @param int $keyId 
         * @return array
         */
        return $this->sendCall($keyId);
    }

    public function addSshKey($name, $sshKey) {
        //TODO
        /* add ssh key
         * @param string $name
         * @param string $sshKey
         * @return bool
         */
        return $this->sendCall('add');
    }

    public function editSshKey($keyId) {
        //TODO
        /* edit ssh key
         * @param string $name
         * @param string $sshKey
         * @return bool
         */
    }

    public function destroySshKey($keyId) {
        /* Delete a ssh key
         * @param int $keyId 
         * @return bool
         */
        return $this->sendCall($keyId . '/destroy');
    }

}

