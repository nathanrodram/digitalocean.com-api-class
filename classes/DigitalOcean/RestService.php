<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author    Nathan Rodriguez nathan.rodram at gmail.com
 * 
 */

namespace DigitalOcean;

abstract class RestService {

    const URL = "https://api.digitalocean.com/";

    private $apiKey;
    private $clientId;
    private $ch;

    public function __construct($apiKey, $clientId) {
        $this->apiKey = $apiKey;
        $this->clientId = $clientId;
    }

    public function sendCall($actions = '', $params = '') {
        /* Send get call
         * @param actions string
         * @param params string
         * @return array
         */
        
        $path = $this->controller;
        if($actions)
        {
            $path .= '/' . $actions; 
        }
        $path .= '/?client_id=' . $this->clientId . '&api_key=' . $this->apiKey . $params;
        
        $url = $this::URL . $path;
        //var_dump($url);die();
        $this->ch = curl_init($url);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_POST, 0);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 120);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        $result = json_decode(curl_exec($this->ch));
        if($result->status == 'OK')
        {
            return $result;
        }
        else
        {
            return false;
        }
    }

}

