<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author    Nathan Rodriguez nathan.rodram at gmail.com
 * 
 */

namespace DigitalOcean;

class Droplets extends \DigitalOcean\RestService {

    protected $controller = 'droplets';

    public function getDroplets() {
        /* List all droplets
         * @return array
         */

        return $this->sendCall();
    }

    public function getDroplet($dropletId) {
        /* Show a single droplet
         * @param int $dropletId;
         * @return array
         */
        return $this->sendCall($dropletId);
    }

    public function createDroplet($dName, $sizeId, $imageId, $regionId) {
        /*  Create a new droplet
         * @param string $dName
         * @param int $sizeId
         * @param int $imageId
         * @param int $regionId
         *  @return array
         */
        $params = '&name=' . $dName . '&size_id=' . $sizeId . '&image_id=' . $imageId . 'region_id=' . $regionId;
        return $this->sendCall('new', $params);
    }

    public function rebootDroplet($dropletId) {

        /* Soft reboot droplet
         * @param int dropletId
         * @return bool
         */

        return $this->sendCall($dropletId . '/reboot');
    }

    public function forceRebootDroplet($dropletId) {
        /* power reboot droplet
         * @param int $dropletId
         * @return bool
         */

        return $this->sendCall($dropletId . '/power_cycle');
    }

    public function shutdownDroplet($dropletId) {
        /* shutdown droplet
         * @param int $dropletId
         * @return bool
         */

        return $this->sendCall($dropletId . '/shutdown');
    }

    public function powerOffDroplet($dropletId) {
        /* power off  droplet
         * @param int $dropletId
         * @return bool
         */

        return $this->sendCall($dropletId . '/power_off');
    }

    public function powerOnDroplet($dropletId) {
        /* power on  droplet
         * @param int $dropletId
         * @return bool
         */

        return $this->sendCall($dropletId . '/power_on');
    }

    public function resetRootPassword($dropletId) {
        /* Reset root password and reboot droplet
         * @param int $dropletId
         * @return bool
         */
        return $this->sendCall($dropletId . '/reset_root_password');
    }

    public function resizeDroplet($dropletId, $sizeId) {
        /* Upgrade/downgrade cpu and memory specs
         * @param int $dropletId
         * @param int $sizeId
         */
        return $this->sendCall($dropletId . '/resize', '&size_id=' . $sizeId);
    }

    public function takeSnapshot($dropletId, $name = NULL) {
        /* Take droplet snapshot(maybe reboot)
         * @param int $dropletId
         * @return bool
         */
        $params = $name ? '&name=' . $name : '';
        return $this->sendCall($dropletId . '/snapshot', $params);
    }

    public function restoreDroplet($imageId) {
        /* Restore a droplet image from snapshot
         * @param int $imageId
         * @return bool
         */
        return $this->sendCall($dropletId . '/restore', '&image_id=' . $imageId);
    }

    public function rebuildDroplet($dropletId) {
        /* Rebuild droplet
         * @param int $dropletId
         * @return bool
         */
        return $this->sendCall($dropletId . '/rebuild', '&image_id=' . $imageId);
    }

    public function enableBackups($dropletId) {
        /* Enable automatic backups
         * @param int $dropletId
         * @return bool
         */
        return $this->sendCall($dropletId . '/enable_backups');
    }

    public function disableBackups($dropletId) {
        /* Disable automatic backups
         * @param int $dropletId
         * @return bool
         */
        return $this->sendCall($dropletId . '/disable_backups');
    }

    public function destroyDroplet() {
        /* Delete droplet
         * @param int $dropletId
         * @return bool
         */
        return $this->sendCall($dropletId . '/destroy');
    }

}

