<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author    Nathan Rodriguez nathan.rodram at gmail.com
 * 
 */

namespace DigitalOcean;

class Images extends \DigitalOcean\RestService {

    protected $controller = 'images';

    public function getImages() {
        /* List available os images
         * @return array
         */
        return $this->sendCall();
    }

    public function getImage($imageId) {
        /* Show image info
         * @param int $imageId 
         * @return array
         */
        return $this->sendCall($imageId);
    }

    public function destroyImage($imageId) {
        /* Delete image
         * @param int $imageId 
         * @return bool
         */
        $response = $this->sendCall($imageId . '/destroy');
        if ($response) {
            return true;
        } else {
            return false;
        }
    }

}